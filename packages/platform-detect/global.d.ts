declare module "*.vue" {
  import { Component } from "vue";
  const T: Component;
  export default T;
}
