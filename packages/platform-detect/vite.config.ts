import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { viteSingleFile } from "vite-plugin-singlefile";
// import legacy from "@vitejs/plugin-legacy";

export default defineConfig({
  build: {
    target: "es2017",
  },
  plugins: [
    vue(),
    viteSingleFile(),
    // legacy({
    //   targets: ["safari 9"],
    // }),
  ],
});
