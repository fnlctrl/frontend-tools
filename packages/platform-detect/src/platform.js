
var ba = {
  "768_1024": !0,
  "744_1133": !0,
  "810_1080": !0,
  "820_1180": !0,
  "834_1112": !0,
  "834_1194": !0,
  "1024_1366": !0,
},
  ya = function () {
    return (
      (window.PointerEvent &&
        "maxTouchPoints" in navigator &&
        navigator.maxTouchPoints > 0) ||
      (window.matchMedia &&
        window.matchMedia("(any-pointer:coarse)").matches)
    );
  },
  ka = function () {
    if (!window.screen || !ya()) return !1;
    var e = window.screen,
      n = e.width,
      t = e.height;
    return ba[n + "_" + t] || ba[t + "_" + n] || !1;
  },
  wa = function () {
    return (
      !(!window.screen || !ya()) &&
      Math.min(window.screen.width, window.screen.height) < 500
    );
  },
  xa = /Windows NT ((?:\d\.?)+)|Windows XP(.*?)/i,
  ja = /OS X ((?:\d+[._]?)+)/,
  Sa = /Android ((?:\d\.?)+)/,
  za = [xa, ja, Sa, /OS ((?:\d+_?)+)/],
  Ca = /Version\/((?:\d+.?)+) (Mobile\/.*?)?Safari/,
  Aa = /(?:Edge|EdgiOS|EdgA)\/((?:\d+.?)+)/,
  Oa = /(?:Edg)\/((?:\d+.?)+)/,
  Pa = /(?:Chrome|CriOS)\/((?:\d+.?)+)/,
  Ta = /(?:Firefox|FxiOS)\/((?:\d+.?)+)/,
  La = /(?:OPR|Opera|OPiOS)\/((?:\d+.?)+)/,
  Za = /(?:OPT)\/((?:\d+.?)+)/,
  Ea = [Ca, Aa, Pa, Ta, La, Za];
function Da(e) {
  for (var n, t = navigator.userAgent, o = _a(e); !(n = o()).done;) {
    var i = n.value,
      a = t.match(i);
    if (a && a[1]) {
      var r = a[1].split(/[_.]/),
        s = r[0],
        u = r[1],
        l = void 0 === u ? "0" : u,
        c = r[2],
        d = void 0 === c ? "0" : c;
      return {
        major: parseInt(s),
        minor: parseInt(l),
        revision: parseInt(d),
      };
    }
  }
  return { major: 0, minor: 0, revision: 0 };
}
var Ia = [
  ["isOpera", La],
  ["isOperaTouch", Za],
  ["isWechat", /micromessenger/i],
  ["isEdge", Aa],
  ["isIE", /msie|trident/i],
  ["isFirefox", Ta],
  ["isQQ", /QQBrowser\/((?:\d+.?)+)/],
  ["isBbAccess", /GoodAccess\//],
  ["isChrome", Pa],
  ["isSafari", Ca],
],
  $a = [
    "XiaoMi/MiuiBrowser",
    "HuaweiBrowser",
    "OppoBrowser",
    "VivoBrowser",
  ],
  Ma = [
    ["isWindows", xa],
    [
      "isAndroid",
      function (e) {
        return (
          Sa.test(e) ||
          $a.some(function (n) {
            return e.includes(n);
          })
        );
      },
    ],
    ["isLinux", /linux/i],
    ["isChromeOS", /CrOS/],
    [
      "isIos",
      function (e) {
        return (
          /ipad|iphone|ipod/i.test(e) || (ja.test(e) && (ka() || wa()))
        );
      },
    ],
    ["isMac", ja],
  ],
  Ua = /UCBrowser/i;
function Na(e, n) {
  for (
    var t, o = navigator.userAgent, i = { version: n }, a = !1, r = _a(e);
    !(t = r()).done;

  ) {
    var s = t.value,
      u = s[0],
      l = s[1],
      c = "function" == typeof l ? l(o) : l.test(o),
      d = !a && c;
    d && (a = !0), (i[u] = d);
  }
  return i;
}

export function platformDetect() {
  var e = navigator.userAgent,
    n = Ua.test(e)
      ? Ia.reduce(
        function (e, n) {
          return (e[n[0]] = !1), e;
        },
        { version: { major: 0, minor: 0, revision: 0 } }
      )
      : Na(Ia, Da(Ea)),
    t = Na(Ma, Da(za)),
    o = t.isMac || t.isWindows || t.isChromeOS || t.isLinux,
    i =
      wa() &&
      (t.isAndroid || t.isIos || /windows phone|blackberry/i.test(e)),
    a = ka() && t.isIos;
  return (
    (n.isChromiumEdge = n.isChrome && Oa.test(e)),
    {
      browser: n,
      os: t,
      device: {
        isDesktop: o,
        isPhone: i,
        isIpad: a,
        isMobile: !o,
        isTablet: a || (!o && !i),
      },
    }
  );
}

function _a(e, n) {
  var t =
    ("undefined" != typeof Symbol && e[Symbol.iterator]) || e["@@iterator"];
  if (t) return (t = t.call(e)).next.bind(t);
  if (
    Array.isArray(e) ||
    (t = (function (e, n) {
      if (e) {
        if ("string" == typeof e) return va(e, n);
        var t = Object.prototype.toString.call(e).slice(8, -1);
        return (
          "Object" === t && e.constructor && (t = e.constructor.name),
          "Map" === t || "Set" === t
            ? Array.from(e)
            : "Arguments" === t ||
              /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t)
              ? va(e, n)
              : void 0
        );
      }
    })(e)) ||
    (n && e && "number" == typeof e.length)
  ) {
    t && (e = t);
    var o = 0;
    return function () {
      return o >= e.length ? { done: !0 } : { done: !1, value: e[o++] };
    };
  }
  throw new TypeError(
    "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
  );
}