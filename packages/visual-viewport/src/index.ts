const vw = window.visualViewport;
const logs = document.getElementById("logs");

vw?.addEventListener("resize", (e) => {
  const { width, height, offsetLeft, offsetTop } = vw;
  const p = document.createElement("p");
  p.innerText = `visual viewport resize: width ${width}, height: ${height}, offsetLeft: ${offsetLeft}, offsetTop: ${offsetTop}`;
  logs?.appendChild(p);
});
